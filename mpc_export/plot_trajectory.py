
import matplotlib.pyplot as plt
import sys


def plot_from(fname):
    lines = tuple(open(fname, 'r'))
    lines = [x.strip() for x in lines]

    time = []
    actual_x = []
    actual_y = []
    actual_orientation = []
    reference_x = []
    reference_y = []
    reference_orientation = []
    velocity_linear = []
    velocity_angular = []


    for i in range(len(lines)):
        # convert to integer and append to the last
        # element of the list
        line = lines[i].split()
        time.append(float(line[0]))
        actual_x.append(float(line[1]))
        actual_y.append(float(line[2]))
        actual_orientation.append(float(line[3]))
        reference_x.append(float(line[4]))
        reference_y.append(float(line[5]))
        reference_orientation.append(float(line[6]))
        velocity_linear.append(float(line[7]))
        velocity_angular.append(float(line[8]))


    plt.plot(reference_x,reference_y,'k--',label='yd')
    plt.plot(actual_x,actual_y,'k',label = 'y')
    plt.plot(actual_x[0],actual_y[0],'ok', label="Initial position")
    plt.title('Desired and realized path')
    plt.legend(loc=0, numpoints=1)
    plt.xlabel('x, xd [m]')
    plt.ylabel('y, yd [m]')
    plt.show()

    plt.plot(time,actual_orientation,'k',label='theta')
    plt.plot(time,reference_orientation,'k--',label='theta_d')
    plt.title('Orientation')
    plt.legend(loc=0, numpoints=1)
    plt.show()

    # print(time)

    plt.plot(time,velocity_linear,'k',label='velocity_linear')
    plt.plot(time,velocity_angular,'k--',label='velocity_angular')
    plt.title('Velocities')
    plt.legend(loc=0, numpoints=1)
    plt.ylim([-0.05, 0.35])
    plt.show()


if __name__ == "__main__":

    if len(sys.argv)>1:
        fname = sys.argv[1]
        plot_from(fname)
    else:
        print("Get file name as a first argument, please!")
