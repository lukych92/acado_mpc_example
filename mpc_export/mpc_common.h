#ifndef MPC_COMMON_H
#define MPC_COMMON_H

#include "acado_common.h"

#ifdef __cplusplus
extern "C"
{
#endif /* __cplusplus */

  __attribute__((visibility("default"))) void MPC_preparationStep();
  __attribute__((visibility("default"))) int MPC_feedbackStep();
  __attribute__((visibility("default"))) int MPC_initializeSolver();
  __attribute__((visibility("default"))) void MPC_initializeNodesByForwardSimulation();
  __attribute__((visibility("default"))) void MPC_shiftStates( int strategy, real_t* const xEnd, real_t* const uEnd );
  __attribute__((visibility("default"))) void MPC_shiftControls( real_t* const uEnd );
  __attribute__((visibility("default"))) real_t MPC_getKKT();
  __attribute__((visibility("default"))) real_t MPC_getObjective();
  __attribute__((visibility("default"))) void MPC_resetSolverMem();

#ifdef __cplusplus
} /* extern "C" */
#endif /* __cplusplus */

#endif /* Close the module */
