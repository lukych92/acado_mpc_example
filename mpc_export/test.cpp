/*
 *    This file was auto-generated using the ACADO Toolkit.
 *
 *    While ACADO Toolkit is free software released under the terms of
 *    the GNU Lesser General Public License (LGPL), the generated code
 *    as such remains the property of the user who used ACADO Toolkit
 *    to generate this code. In particular, user dependent data of the code
 *    do not inherit the GNU LGPL license. On the other hand, parts of the
 *    generated code that are a direct copy of source code from the
 *    ACADO Toolkit or the software tools it is based on, remain, as derived
 *    work, automatically covered by the LGPL license.
 *
 *    ACADO Toolkit is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */
/*

IMPORTANT: This file should serve as a starting point to develop the user
code for the OCP solver. The code below is for illustration purposes. Most
likely you will not get good results if you execute this code without any
modification(s).

Please read the examples in order to understand how to write user code how
to run the OCP solver. You can find more info on the website:
www.acadotoolkit.org

*/

#include "mpc_common.h"
#include "mpc_auxiliary_functions.h"

#include <stdio.h>
#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <sstream>
#include <iomanip>      // std::setw


/* Some convenient definitions. */
#define NX          ACADO_NX  /* Number of differential state variables.  */
//#define NXA         ACADO_NXA /* Number of algebraic variables. */
#define NU          ACADO_NU  /* Number of control inputs. */
//#define NOD         ACADO_NOD  /* Number of online data values. */

#define NY          ACADO_NY  /* Number of measurements/references on nodes 0..N - 1. */
#define NYN         ACADO_NYN /* Number of measurements/references on node N. */

#define N           ACADO_N   /* Number of intervals in the horizon. */

//#define NUM_STEPS   10        /* Number of real-time iterations. */

/* Global variables used by the solver. */
real_t *aVy = MPC_getAcadoVariablesY();//;acado_getAcadoVariablesY();
real_t *aVyN = MPC_getAcadoVariablesYN();
real_t *aVx = MPC_getAcadoVariablesX();
real_t *aVx0 = MPC_getAcadoVariablesX0();
real_t *aVu = MPC_getAcadoVariablesU();


class orocos_class
{

public:

    std::string refTrackFile;
    double numSteps; //number of controller steps
    std::vector< std::vector< double > > reference_trajectory;
    std::vector< std::vector< double > > logs; //actual and reference position log
    std::vector< std::vector< double > > actual_state;


    orocos_class(): refTrackFile("reference_trajectory.txt")
    {

    }


    bool init_func(void)
    {
        MPC_resetSolverMem();
        std::cout << "INIT!" <<std::endl;
        if (!readDataFromFile(refTrackFile.c_str(), reference_trajectory))
        {
            std::cout << "Cannot read refrence trajectory" << std::endl;
            return false;
        }
        else
        {
            std::cout << "Refrence trajectory read from " << refTrackFile.c_str() << std::endl;
            numSteps=reference_trajectory.size();
            std::cout << numSteps << std::endl;
//      std::cout << reference_trajectory[0][0] << std::endl;
        }

        std::string acutal_state_name = "actual_state.txt";
        if (!readDataFromFile(acutal_state_name.c_str(), actual_state))
        {
            std::cout << "Cannot read actual_state" << std::endl;
            return false;
        }
        else
        {
            std::cout << "ass" << actual_state.size() << std::endl;
        }

        logs.resize(numSteps);
        for (int i = 0; i < logs.size(); ++i)
            logs[ i ].resize(9, 0.0); //x,y,phi actual and reference, controls

        printf("NX %d,NU %d,NY %d,NYN %d,N %d\n",NX,NU,NY,NYN,N);



        //Initialize solver
        if(MPC_initializeSolver() != 0){
            std::cout << "Solver initialisation failed!" << std::endl;
            return false;
        }


        // Prepare a consistent initial guess
//        for (int i = 0; i < N + 1; ++i) //read states from file
//            for (int j=0; j <= NX; ++j)
//                aVx[i * NX + j] = 0.0;
//
//        for (int i = 0; i < N; ++i)  //read controls from file
//            for (int j=1; j <= NU; ++j)
//                aVu[i * NU + j] = 0.0;//reference_trajectory[ i ][ NX + j ];

        // Prepare references
        for (int i = 0; i < N; ++i)
        {
            aVy[i * NY + 0] = reference_trajectory[ i ][ 1 ]; // x
            aVy[i * NY + 1] = reference_trajectory[ i ][ 2 ]; // y
            aVy[i * NY + 2] = reference_trajectory[ i ][ 3 ]; // theta
            aVy[i * NY + 3] = 0;//reference_trajectory[ i ][ 4 ]; // u1
            aVy[i * NY + 4] = 0;//reference_trajectory[ i ][ 5 ]; // u2
        }
        // Current state feedback
        for (int i = 0; i < NX; ++i)
            aVx0[ i ] = aVx[ i ];
        // Position logging initialisation

//        printStep(0);

        // Warm-up the solver
        MPC_preparationStep();

    }

    bool update_func(int iter)
    {
        // In this simple example, we feed the NMPC with an ideal feedback signal
        // i.e. what NMPC really expects in the next sampling interval
        for (int i = 0; i < NX; ++i)
            aVx0[ i ] = aVx[NX + i];

        /* Perform the feedback step. */
        int status;
        status = MPC_feedbackStep();
        if(status != 0 ){
            std::cout << "QP problem! QP status: "
                      << status << std::endl;
            return false;
        }
        /* Apply the new control immediately to the process, first NU components. */

  //      printStep(iter);

        logs[iter][0] = iter*0.02; // time
        logs[iter][1] = aVx[0]; // actual x
        logs[iter][2] = aVx[1]; // actual y
        logs[iter][3] = aVx[2]; // actual theta
        logs[iter][4] = aVy[0]; // reference x
        logs[iter][5] = aVy[1]; // reference y
        logs[iter][6] = aVy[2]; // reference z
        logs[iter][7] = aVu[0];//controls.data[0];
        logs[iter][8] = aVu[1];//controls.data[1];

        /* Optional: shift the initialization (look at mpc_common.h). */
        /* mpc_shiftStates(2, 0, 0); */
        /* mpc_shiftControls( 0 ); */

        /* Prepare for the next step. */




        // Read new references
        for (int i = 0; i < N; ++i){
            if((iter + 1 + i) < numSteps){
                aVy[i * NY + 0] = reference_trajectory[ iter + 1 + i ][ 1 ]; // x
                aVy[i * NY + 1] = reference_trajectory[ iter + 1 + i ][ 2 ]; // y
                aVy[i * NY + 2] = reference_trajectory[ iter + 1 + i ][ 3 ]; // theta
            }
            else{
                aVy[i * NY + 0] = reference_trajectory[ numSteps - 1 ][ 1 ]; // x
                aVy[i * NY + 1] = reference_trajectory[ numSteps - 1 ][ 2 ]; // y
                aVy[i * NY + 2] = reference_trajectory[ numSteps - 1 ][ 3 ]; // theta
            }
            aVy[i * NY + 3] = 0; // u1
            aVy[i * NY + 4] = 0; // u2
        }

//        MPC_shiftStates(2, 0, 0);
        MPC_preparationStep();

    }


    bool readDataFromFile( const char* fileName, std::vector< std::vector< double > >& data )
    {
        std::ifstream file( fileName );
        std::string line;

        if ( file.is_open() )
        {
            while( getline(file, line) )
            {
                std::istringstream linestream( line );
                std::vector< double > linedata;
                double number;

                while( linestream >> number )
                {
                    linedata.push_back( number );
                }

                data.push_back( linedata );
            }

            file.close();
        }
        else
            return false;

        return true;
    }

    void printStep(int iter)
    {
        int i=0;
        std::cout << "Iteration #" << std::setw( 4 ) << iter
                  << ", KKT value: " << std::scientific << MPC_getKKT()
                  << ", objective value: " << std::scientific << MPC_getObjective()
                  << std::endl;

        printf("\tControls: ");
        for (i = 0; i < NU; ++i)
            printf("%.2f ",aVu[i]);
        printf("\n");
        printf("\tVar: ");
        for (i = 0; i < NX; ++i)
            printf("%.2f ",aVx[i]);
        printf("\n");
        printf("\tVar0: ");
        for (i = 0; i < NX; ++i)
            printf("%.2f ",aVx0[i]);
        printf("\n");
        printf("\tMessu: ");
        for (i = 0; i < NY; ++i)
            printf("%.2f ",aVy[i]);
        printf("\n");
        printf("\tRef: ");
        for (i = 0; i < NYN; ++i)
            printf("%.2f ",aVyN[i]);
        printf("\n");
    }

    void save_logs(std::string file_name, int iter)
    {
        std::ofstream dataLog( file_name.c_str() );
        if ( dataLog.is_open() ){
            for (int i = 0; i < iter; i++){
                for (int j = 0; j < logs[ i ].size(); j++)
                    dataLog << logs[ i ][ j ] << " ";
                dataLog << std::endl;
            }
            dataLog.close();
            std::cout << "Log file saved as " << file_name << std::endl;
        }
        else
            std::cout << "Log file could not be opened" << std::endl;
    }

};



int main( )
{
    /* Some temporary variables. */
    int    iter = 0;
    acado_timer t;




    orocos_class oro;

    oro.init_func();

    /* Get the time before start of the loop. */
    mpc_tic( &t );


    printf("\nStart of the RTI loop.\n\n");
    /* The "real-time iterations" loop. */


    while(1)
//    for(iter=0;iter<5;++iter)
    {
        if(iter >= oro.numSteps)
        {
            std::cout << "We are done" << std::endl;
            break;
        }
        oro.update_func(iter);
        iter++;
    }
    printf("\nEnd of the RTI loop.\n\n");

    /* Read the elapsed time. */
    real_t te = mpc_toc( &t );

    std::cout << "Average time of one real-time iteration:   "<< 1e3 * te / iter << " miliseconds" << std::endl;

    oro.save_logs("logs.log",iter);
//	mpc_printDifferentialVariables();
//	mpc_printControlVariables();

    return 0;
}
