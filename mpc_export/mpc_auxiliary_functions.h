#ifndef MPC_AUXILIARY_FUNCTIONS_H
#define MPC_AUXILIARY_FUNCTIONS_H

#include "mpc_common.h"
#include "acado_auxiliary_functions.h"

#define DLL_PUBLIC __attribute__ ((visibility ("default")))

#ifdef __cplusplus
extern "C"
{
#endif /* __cplusplus */


  DLL_PUBLIC real_t* MPC_getAcadoVariablesX( );
  DLL_PUBLIC real_t* MPC_getAcadoVariablesU( );
  DLL_PUBLIC real_t* MPC_getAcadoVariablesY( );
  DLL_PUBLIC real_t* MPC_getAcadoVariablesYN( );
  DLL_PUBLIC real_t* MPC_getAcadoVariablesX0( );
  DLL_PUBLIC real_t* MPC_getAcadoVariablesOD( );

  DLL_PUBLIC void mpc_tic(acado_timer* t);
  DLL_PUBLIC real_t mpc_toc(acado_timer* t);
  DLL_PUBLIC void mpc_printDifferentialVariables();
  DLL_PUBLIC void mpc_printControlVariables();
  DLL_PUBLIC int mpc_getNWSR( void );


#ifdef __cplusplus
} /* extern "C" */
#endif /* __cplusplus */

#endif /* MPC_AUXILIARY_FUNCTIONS_H */
