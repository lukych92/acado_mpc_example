#include <acado_gnuplot.hpp>
#include <acado_toolkit.hpp>

USING_NAMESPACE_ACADO

int main( )
{

//     Define the dynamic system:
//     -------------------------
    DifferentialState     q1,q2,q3;
    Control               u1,u2;

//    Simple monocycle
    DifferentialEquation  f;
    f << dot(q1) == cos(q3)*u1;
    f << dot(q2) == sin(q3)*u1;
    f << dot(q3) == u2;
//
    // DEFINE LEAST SQUARE FUNCTION:
//    // -----------------------------
    Function h, hN;

    h << q1 << q2 << q3 << u1 << u2;
    hN << q1 << q2 << q3;

    DMatrix Q = eye<double>( h.getDim() );
    DMatrix QN = eye<double>( hN.getDim() );

    Q(0,0)=QN(0,0)=100;
    Q(1,1)=QN(1,1)=100;
    Q(2,2)=QN(2,2)=10;
    Q(3,3) = 0.005;
    Q(4,4) = 0.005;


    DVector r(3);
    r.setAll( 0.0 );

//
//    // DEFINE AN OPTIMAL CONTROL PROBLEM:
//    // ----------------------------------
    const double t_start = 0.0;
    const double t_end   = 1.0;
    const int N = 50;

    OCP ocp        ( t_start, t_end, N);
    ocp.subjectTo  ( f );
    ocp.minimizeLSQ(Q, h, r);
    ocp.minimizeLSQEndTerm(QN, hN);

    ocp.subjectTo( 0 <= u1 <= 0.3 );
    ocp.subjectTo( 0 <= u2 <= 0.3 );


    // SETTING UP THE (SIMULATED) PROCESS:
    // -----------------------------------
    OutputFcn identity;
    DynamicSystem dynamicSystem( f,identity );

    Process process( dynamicSystem,INT_RK45 );

    // SETTING UP THE MPC CONTROLLER:
    // ------------------------------
    RealTimeAlgorithm alg( ocp,0.02 );
    alg.set( MAX_NUM_ITERATIONS, 2 );

    StaticReferenceTrajectory zeroReference("mpc_export/reference_trajectory.txt");

    Controller controller( alg,zeroReference );


    // SETTING UP THE SIMULATION ENVIRONMENT,  RUN THE EXAMPLE...
    // ----------------------------------------------------------
    SimulationEnvironment sim( 0.0,26.0,process,controller );

    DVector x0(3);
    x0(0) = 0.0;
    x0(1) = 0.0;
    x0(2) = 0.0;

    if (sim.init( x0 ) != SUCCESSFUL_RETURN)
        exit( EXIT_FAILURE );
    if (sim.run( ) != SUCCESSFUL_RETURN)
        exit( EXIT_FAILURE );

    // ...AND PLOT THE RESULTS
    // ----------------------------------------------------------
    VariablesGrid sampledProcessOutput;
    sim.getSampledProcessOutput( sampledProcessOutput );

    VariablesGrid feedbackControl;
    sim.getFeedbackControl( feedbackControl );

    GnuplotWindow window;
    window.addSubplot( sampledProcessOutput(0), "q1 [m]" );
    window.addSubplot( sampledProcessOutput(1), "q2 [m]" );
    window.addSubplot( sampledProcessOutput(2), "q3 [rad]" );
    window.addSubplot( feedbackControl(1),      "u1 [m/s]" );
    window.addSubplot( feedbackControl(0),      "u2 [rad/s]" );
    window.plot( );

    return 0;
}



