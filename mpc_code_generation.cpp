#include <acado_gnuplot.hpp>
#include <acado_toolkit.hpp>

USING_NAMESPACE_ACADO

int main( )
{
//     Define the dynamic system:
//     -------------------------
    DifferentialState     q1,q2,q3;
    Control               u1,u2;

//    Simple monocycle
    DifferentialEquation  f;
    f << dot(q1) == cos(q3)*u1;
    f << dot(q2) == sin(q3)*u1;
    f << dot(q3) == u2;
//
    // DEFINE LEAST SQUARE FUNCTION:
//    // -----------------------------
    Function h, hN;

    h << q1 << q2 << q3 << u1 << u2;
    hN << q1 << q2 << q3;

    DMatrix Q = eye<double>( h.getDim() );
    DMatrix QN = eye<double>( hN.getDim() );

    Q(0,0)=QN(0,0)=100;
    Q(1,1)=QN(1,1)=100;
    Q(2,2)=QN(2,2)=10;
    Q(3,3) = 0.005;
    Q(4,4) = 0.005;


    DVector r(3);
    r.setAll( 0.0 );

//
//    // DEFINE AN OPTIMAL CONTROL PROBLEM:
//    // ----------------------------------
    const double t_start = 0.0;
    const double t_end   = 1.0;
    const int N = 50;

    OCP ocp        ( t_start, t_end, N);
    ocp.subjectTo  ( f );
    ocp.minimizeLSQ(Q, h);
    ocp.minimizeLSQEndTerm(QN, hN);

    ocp.subjectTo( 0 <= u1 <= 0.3 );
    ocp.subjectTo( 0 <= u2 <= 0.3 );

    // Export the code:
    OCPexport mpc( ocp );

    mpc.set(HESSIAN_APPROXIMATION, GAUSS_NEWTON);
    mpc.set(DISCRETIZATION_TYPE, MULTIPLE_SHOOTING);

    mpc.set(INTEGRATOR_TYPE, INT_IRK_RIIA3);
//    mpc.set(NUM_INTEGRATOR_STEPS, N * Ni);

    mpc.set(SPARSE_QP_SOLUTION, FULL_CONDENSING);
//	mpc.set(SPARSE_QP_SOLUTION, CONDENSING);
    mpc.set(QP_SOLVER, QP_QPOASES);
//	mpc.set(MAX_NUM_QP_ITERATIONS, 20);
    mpc.set(HOTSTART_QP, YES);

    mpc.set( GENERATE_TEST_FILE,          NO             );
    mpc.set( GENERATE_MAKE_FILE,          NO             );
    mpc.set( GENERATE_MATLAB_INTERFACE,   NO             );
    mpc.set( GENERATE_SIMULINK_INTERFACE, NO             );


    if (mpc.exportCode( "mpc_export" ) != SUCCESSFUL_RETURN)
        exit( EXIT_FAILURE );
    else
    {
        std::cout << "Code generated to folder mpc_export" << std::endl;
        mpc.printDimensionsQP();
    }


    return 0;
}

