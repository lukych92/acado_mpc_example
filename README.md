# README #

An example of the model predictive control code generation. Kinematic monocycle model is used.


At the beginning, you should install ACADOtoolkit from page [http://acado.github.io/install_linux.html](Link URL) and source ACADO with the following command 


```
#!bash

source ~/ACADOtoolkit/build/acado_env.sh
```


Compilation of the example

```
#!bash

mkdir build
cd build
cmake ..
make
cd ..

```

Code generation and test generated code

```
#!bash

./mpc_code_generation
cd mpc_mpc_export
make
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$(pwd)
./test.sh
```


Simulation
```
#!bash

./mpc_simulation
```